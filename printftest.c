/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printftest.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/22 12:18:47 by dfarhi            #+#    #+#             */
/*   Updated: 2021/11/23 22:58:04 by davifah          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <limits.h>
#include <stdio.h>

int	main(void)
{
	int	res;

	res = ft_printf("%i%d%c%s%p%u%x%X%%", 0, -10, 'a', "2", &res, 10);
	ft_putchar_fd('\n', 1);
	ft_putnbr_fd(res, 1);
	ft_putchar_fd('\n', 1);
}
