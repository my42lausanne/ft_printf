# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dfarhi <dfarhi@student.42lausanne.ch>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/10/11 12:17:38 by dfarhi            #+#    #+#              #
#    Updated: 2021/11/23 22:53:53 by davifah          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SRC_FILES = ft_printf.c manage_fi.c int_format.c uint_format.c ft_putlong_fd.c \
			char_format.c str_format.c ft_itoa_base.c hex_format.c \
			ptr_format.c ft_lluitoa_base.c

SRCS	= $(addprefix src/, ${SRC_FILES})

OBJS	= ${SRCS:.c=.o}

NAME	= libftprintf.a

CC		= gcc -Wall -Wextra -Werror

AR		= ar rcs

.c.o:
			${CC} -c -I./includes $< -o ${<:.c=.o}

${NAME}:	libft ${OBJS}
			${AR} ${NAME} ${OBJS}

all:		${NAME}

libft:		
			make -C ./libft bonus
			cp libft/libft.a ${NAME}

test:		${NAME}
			gcc -I./includes printftest.c -o test -L. -lftprintf

clean:
			rm -f ${OBJS}
			make -C ./libft clean

# "Force clean" remove all compiled files
fclean:		clean
			rm -f ${NAME} libft/libft.a test

# Rule to recompile
re:			fclean all

# Rules that do not have files, if those files exist, they are ignored
.PHONY:		all clean fclean re libft
